# 1. Accept a year input from the user and determine if it s a leap year or not.
# validations:
	# strings are not allowed for inputs
	# no zero or negative values

year = input("Please input a year:\n");

if not year.isnumeric():
	print("String or negative value is not allowed")
elif int(year) <= 0:
	print("Zero or negative value is not allowed")
elif int(year) % 4 == 0 and (int(year) % 100 != 0 or int(year) % 400 == 0):
	print(f"{year} is a leap year")
else:
	print(f"{year} is not a leap year")

# 2. Accept two numbers(row and col) from the user and create a grid of asterisk using the two numbers (row and col)

row = int(input("Enter number of rows\n"))
col = int(input("Enter number of columns\n"))

i = 0
while i < row:
	newString = ""
	i += 1
	j = 0
	while j < col:
		newString += "*"
		j += 1
	print(newString)

	